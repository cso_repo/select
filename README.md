# README #

This is the official repository of the R package SELECT - Selected Events Linked by Evolutionary Conditions across human Tumors - a methodology to infer evolutionary inter-dependencies between functional alterations in cancer.

### What is this repository for? ###

* SELECT is a package for inferring evolutionary dependencies between functional alterations in cancer starting from patterns of alteration occurrences in large tumor sample cohorts.
* Last Version: 1.6.1

### How To ###

* Summary of set up

Install the package with R CMD INSTALL [select tarball file name]

* How to run tests

Run the example test test_random_v1.R in folder test.

### Contribution guidelines ###

SELECT was originally developed by Marco Mina at the University of Lausanne, Switzerland.
SELECT is currently maintained by the Computational Systems Oncology lab (CSO) at the University of Lausanne (Unil), Switzerland.

### Who do I talk to? ###

* For any question related to SELECT, please contact Marco Mina (marco.mina.85@gmail.com) or Giovanni Ciriello (giovanni.ciriello@unil.ch).
